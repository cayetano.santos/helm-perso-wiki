;;; hpw.el --- Very simple personal wiki. -*- lexical-binding: t; -*-
;;
;; Inspired by helm-org-wiki
;;
;; Author		: Cayetano Santos
;; Homepage		: https://gitlab.com/csantosb/helm-perso-wiki
;; Documentation	: https://csantosb.gitlab.io/helm-perso-wiki/
;; Package-Version	: 1.8
;; Package-Requires	: ((deft "0.8") (projectile "0.14.0") (helm-core "2.9.8") (perspective "1.12") (f "1.18.1") (toc-org "1.0.0") (dash "2.14.0") (emacs "26"))
;; Created		: 2014/11/21
;; Keywords		: wiki, helm, deft, projectile, perspective, gitit
;;
;;; License
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; Implements a minor mode to deal with a very simple personal wiki, providing
;; a way to navigate a hierarchy of categories and pages (wiki items).
;;
;; The wiki may exist in parallel to other project files (to document them, for
;; example). The minor mode ignores files not considered as wiki items.
;;
;; The navigating facility provided here extends and is based on the combined
;; use of `helm', `perspective' and `projectile'. Finally, the wiki structure
;; aims at being compatible with `deft' and `gitit'.
;;
;;; Keys:
;;
;; Minor mode keymap
;;
;; Hydra:
;;
;;   C-ç w	:: call helm launcher
;;       p	:: select pages below current level (with a prefix argument, select all pages)
;;       c	:: select categories below current level (with a prefix argument, select all pages)
;;       i	:: find index pages
;;
;; Global map (suggested)
;;
;;   C-x c w a	:: add current project to list of wiki projects, enabling minor mode
;;
;; with
;;
;; (define-key helm-command-map (kbd "wa") 'hpw-add-project)
;;
;;; Documentation:
;;
;; Refer to this project documentation wiki for details
;;
;;   https://csantosb.gitlab.io/helm-perso-wiki/
;;
;;; Code:

;;; Requires

(require 'helm)
(require 'persp-projectile)
(require 'helm-projectile)
(require 'deft)
(require 'f)
(require 'dash)
(require 'yasnippet)

;;; Variables

;;;; To bet set by the user

(defvar hpw-wikis '("my-perso-wiki")
  "Wiki projects.")

(defvar hpw-debug-mode t "")

(defvar hpw-category-extension "cat-extension"
  "Extension or files for categories.")

(defvar hpw-subcategory-extension "page-extension"
  "Extension or files for subcategories.
Gitit will only recognize this extension.")

(defvar hpw-back-tip "backtip"
  "Tip to be shown in the list of wiki items to get back (up) one level." )

(defvar hpw-top-tip "top-tip"
  "Tip to be shown in the list of wiki items to get to the top level.")

(defvar hpw-cat-tip " ❯❯ "
  "Tip to be shown in the list of wiki items to identify categories.")

(defvar hpw-fuzzy-match nil
  "Use fuzzy matching.")

;;;; Internal

(defvar hpw--pages-header "---\nformat: Org\ncategories: $1\ntoc: yes\ntitle: $2\ncontent: $3\n...\n\n" "")

(defvar hpw--toc-header "* Table of Contents        :TOC:noexport:\n:PROPERTIES:\n:VISIBILITY: all\n:END:\n\n* $4\n\n" "")

(defvar hpw--directory "" "Directory where files for `hpw' are stored, including trailing slash."  )

(defvar hpw--items-all nil "Logical to select all wiki items.")

(defvar hpw-help-message-not-found
  "* Helm Perso Wiki Help

When no candidate item is selected, it is possible to create a new item,
category or page. Just enter its name and extension.

When a new category is created, an index file is automatically created too
containing a header and elisp code for generating a table of contents of the new
category. See =Toc= under reference below.

In the case of a new page, a template fills up header fields for the user.

** References

For more details see:

    https://csantosb.gitlab.io/helm-perso-wiki")

(defvar hpw-help-message-wiki
  "* Helm Perso Wiki Help

Call =hpw-launcher= with =w= to browse the wiki items recursively.

With a prefix argument =C-u=, browse from the top (default is current level).

Currently, it is possible to browse to the top, upper level and selected
category, as well as to open the selected page.

All usual helm keys are available, with a few overloaded.

Actions are:

+ ~Get to~ (default), open pages, browse to categories and change level

+ ~Open deft here~, launch deft at the current level, =C-c C-d= or =f2=

+ ~Browse pages~, falls back to =hpw-pages=, =C-c C-p= or =f3=

  - when in a category, show pages below it

  - when in a page, a top or a back tip, show pages at current level and below

  - with a prefix argument, show all pages from top level

+ ~Browse Categories~, falls back to =hpw-categories=, =C-c C-c= or =f4=

  - when in a category, show categories below it

  - when in a page, a top or back tip, show categories at current level and
    below

  - with a prefix argument, show all categories from top level

+ ~Index page~, creates a new index page or opens existing at current level,
  =C-c C-i= or =f5=

  - with a prefix argument, open index at top level

** References

For more details see:

    https://github.com/csantosb/hpw/wiki/Use ")

;;; Core functions

;; These functions provide the logic to to present pages, categories
;; and wiki items. They will select wiki items following the value of
;; `hpw--items-all' and `hpw--directory'

;; To access them, use the provided `hpw-item' interface functions

;;;; Helm Projectile Find Page

;; copy-paste of `helm-source-projectile-files-list'

;;;;; Defun
(helm-projectile-command "find-pages" hpw-pages-list "Find page: " t)

;;;;; Keymap
(setq hpw-pages-lis-map (copy-keymap helm-projectile-find-file-map))
;; add a key to jumpt to categories
(define-key hpw-pages-lis-map (kbd "C-c M-c") #'hpw-source-action-browse-category)

;;;;; Actions
(setq hpw-pages-list-actions (copy-alist helm-projectile-file-actions))
;; add action to jump to category
(setq hpw-pages-list-actions (append hpw-pages-list-actions '(( "Browse categories" . hpw-source-action-browse-category))))

;;;;; Source
(defvar hpw-pages-list
  (helm-build-in-buffer-source "Perso wiki pages (press C-c ? for query help)"
    :data (lambda ()
	    (let ((list-of-pages
		   (condition-case nil
		       (-mapcat 'hpw-pages-filter-function
				(projectile-current-project-files))
		     (error nil))))
	      ;; Append a tip to be used to get to the upper level
	      (unless hpw--items-all
		(push hpw-back-tip list-of-pages))
	      ;; Append a tip to be used to get to the top level
	      (unless hpw--items-all
		(push hpw-top-tip list-of-pages))
	      list-of-pages ))
    :fuzzy-match hpw-fuzzy-match
    :coerce 'helm-projectile-coerce-file
    :keymap hpw-pages-lis-map
    :help-message 'helm-ff-help-message
    ;; :mode-line helm-ff-mode-line-string
    :action hpw-pages-list-actions )
  "Helm source definition for wiki pages.")

;; Filter
(defun hpw-pages-filter-function(x)
  (when (and
	 ;; must end by page suffix
	 (string= (f-ext x) hpw-subcategory-extension)
	 ;; must be 'all' or contain hpw--directory in its name
	 (or hpw--items-all
	     (string-prefix-p (expand-file-name hpw--directory)
			      (format "%s%s" (projectile-project-root) x))))
    (list x)))

;;;; Helm Projectile Find Category

;; copy-paste of `helm-source-projectile-directories-list'
;; TODO: see test code

;;;;; Defun
(helm-projectile-command "find-category" hpw-category-list "Find category: " t)

;;;;; Keymap

(defvar hpw-category-list-map
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map helm-map)
    (helm-projectile-define-key map
      (kbd "<left>") 'helm-previous-source
      (kbd "<right>") 'helm-next-source
      (kbd "C-c o") 'helm-projectile-dired-find-dir-other-window
      (kbd "M-e")   'helm-projectile-switch-to-eshell
      (kbd "C-c f") 'helm-projectile-dired-files-new-action
      (kbd "C-c a") 'helm-projectile-dired-files-add-action
      (kbd "C-s")   'helm-find-files-grep
      ;;
      (kbd "C-c M-w") (lambda () (hpw-wiki nil))
      (kbd "C-c M-p") (lambda () (hpw-source-action-browse-pages))
      (kbd "C-c M-i") (lambda ()
			(let ((hpw--directory
			       default-directory))
			  (hpw-source-action-index-page))))
    map)
  "bla." )

;;;;; Actions
(defvar hpw-category-list-actions
  (helm-make-actions "Open Dired" 'helm-projectile-dired-find-dir
		     "Open Dired in other window `C-c o'" 'helm-projectile-dired-find-dir
		     "Switch to Eshell `M-e'" 'helm-projectile-switch-to-eshell
		     "Grep in projects `C-s'" 'helm-projectile-grep
		     "Create Dired buffer from files `C-c f'" 'helm-projectile-dired-files-new-action
		     "Add files to Dired buffer `C-c a'" 'helm-projectile-dired-files-add-action
		     "Browse pages `C-c C-p'" 'hpw-source-action-browse-pages))

;;;;; Source
(defvar hpw-category-list
  (helm-build-in-buffer-source "Perso wiki categories (press C-c ? for query help)"
    ;; List all wiki items under hpw--directory
    :data (lambda () (let ((list-of-categories
		       (condition-case nil
			   (-mapcat 'hpw-categories-filter-function
				    (projectile-current-project-dirs))
			 (error nil))))
		  ;; Append a tip to be used to get to the upper level
		  (unless hpw--items-all
		    (push hpw-back-tip list-of-categories))
		  ;; Append a tip to be used to get to the top level
		  (unless hpw--items-all
		    (push hpw-top-tip list-of-categories))
		  list-of-categories ))
    :fuzzy-match hpw-fuzzy-match
    :coerce (lambda (candidate)
	      (with-current-buffer (helm-candidate-buffer)
		(format "%s.cat/" (expand-file-name
				   (format "%s%s" hpw--directory (replace-regexp-in-string hpw-cat-tip "" candidate))
				   (projectile-project-root)))))
    ;; :action-transformer 'helm-find-files-action-transformer
    :keymap hpw-category-list-map
    :help-message 'helm-ff-help-message
    :mode-line helm-read-file-name-mode-line-string
    :action hpw-category-list-actions)
  "Helm source for listing wiki categories.")

;; Filter
(defun hpw-categories-filter-function(x)
  (let ((x (f-expand x (projectile-project-root))))
    (when (and
	   ;; must end by category suffix
	   (string= (f-ext (directory-file-name x))
		    hpw-category-extension)
	   ;; one of
	   (or hpw--items-all
	       (f-child-of-p x hpw--directory)))
      ;; give a format before sending
      (list (format "%s%s" hpw-cat-tip (replace-regexp-in-string
					".cat$" ""
					(f-relative x hpw--directory)))))))

;;;; Helm Projectile Find wiki

;;;;; Defun
(defun helm-projectile-find-wiki (&optional arg)
  "Start helm with items of the wiki in optional DIRECTORY folder."
  (interactive "P")
  (when (projectile-project-p)
    (projectile-maybe-invalidate-cache arg))
  (helm :sources '(hpw-wiki-list
		   hpw-wiki-list-not-found)
	:buffer "*hpw*"
	:full-frame nil
	:persistent-action 'hpw-persistent-action
	:persistent-help "Jump to line (`C-u' Record in mark ring)"
	:candidate-number-limit 20))

;;;;; Keymap

(defvar hpw-map
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map helm-map)
    (define-key map (kbd "C-c M-d") (lambda ()(interactive)(helm-select-nth-action 1))) ;; deft
    (define-key map (kbd "C-c M-p") (lambda ()(interactive)(helm-select-nth-action 2))) ;; pages
    (define-key map (kbd "C-c M-c") (lambda ()(interactive)(helm-select-nth-action 3))) ;; cats
    (define-key map (kbd "C-c M-i") (lambda ()(interactive)(helm-select-nth-action 4))) ;; index
    ;; (helm-projectile-define-key map
    ;;   (kbd "C-c M-d") (lambda (arg)(helm-select-nth-action 1))
    ;;   (kbd "C-c M-p") (lambda (arg)(helm-select-nth-action 2))
    ;;   (kbd "C-c M-c") (lambda (arg)(helm-select-nth-action 3))
    ;;   (kbd "C-c M-i") (lambda (arg)(helm-select-nth-action 4)))
    map)
  "Bla." )

;;;;; Actions

(defun hpw-source-action-get-to (candidate)
  (cond
   ;; Categories: get into
   ((string-prefix-p hpw-cat-tip candidate)
    (hpw-wiki nil
	      (format "%s%s.%s/"
		      hpw--directory
		      (replace-regexp-in-string
		       hpw-cat-tip ""
		       candidate)
		      hpw-category-extension)))
   ;; Pages: open page
   (t (find-file (format "%s%s.%s" hpw--directory candidate
			 hpw-subcategory-extension)))))

(defvar hpw-wiki-list-actions
  (helm-make-actions "Get to" 'hpw-source-action-get-to
		     "Open deft here" 'hpw-source-action-open-deft-here
		     "Browse Pages" 'hpw-source-action-browse-pages
		     "Browse Categories" 'hpw-source-action-browse-category
		     "Index page" 'hpw-source-action-index-page))

;;;;; Source
(defvar hpw-wiki-list
  ;; (helm-build-in-buffer-source "Perso Wiki (press C-c ? for query help)"
  (helm-build-sync-source "Perso Wiki (press C-c ? for query help)"
    :candidates (lambda ()
		  "Return pages and categories (wiki items) under `hpw--directory'."
		  (let* ((default-directory (if hpw--items-all
						(projectile-project-root)
					      hpw--directory)) ;; necessary because of `file-expand-wildcards'
			 ;; append categories
			 (candidates
			  (mapcar (lambda(x) (format "%s%s" hpw-cat-tip x))
				  (mapcar #'file-name-sans-extension
					  (file-expand-wildcards (format "*.%s" hpw-category-extension)))))
			 ;; append pages
			 (candidates
			  (append candidates
				  (mapcar #'file-name-sans-extension
					  (file-expand-wildcards
					   (format "*.%s" hpw-subcategory-extension))))))
		    ;; Append a tip to be used to get to the upper level
		    (unless (string= hpw--directory (projectile-project-root))
		      (push hpw-back-tip candidates))
		    ;; Append a tip to be used to get to the top level
		    (unless (string= hpw--directory (projectile-project-root))
		      (push hpw-top-tip candidates))
		    ;; return the candidates
		    candidates))
    :fuzzy-match hpw-fuzzy-match
    :keymap hpw-map
    :help-message hpw-help-message-wiki
    :action hpw-wiki-list-actions)
  "Helm source for listing wiki items.")

;; when no candidate is found (no match), propose to create a new one based on the pattern entered
;;
;;   - *.page		:: create new page
;;   - *.cat		:: create new category
;;   - anything else	:: issue an error message

;; Source
(defvar hpw-wiki-list-not-found
  (helm-build-dummy-source "Create (Sub) Category (*.[page|cat]) (press C-c ? for query help)"
    :help-message hpw-help-message-not-found
    :action '(("Create" . hpw-source-not-found-action-create ))))

;;; Interface functions

;;;; Macro

;; This macro generates all `hpw-item' interface functions, which will
;; use the core functions to generate the list of wiki items

(defmacro hpw-item(wiki-item)
  `(defun ,(intern (format "hpw-%s" wiki-item))(arg &optional start-directory)
     "Calls the core functions, setting `hpw--items-all' and `hpw--directory'."
     (interactive "P")
     (let* ((func-to-call (quote ,(intern (format "helm-projectile-find-%s" wiki-item))))
	    (action-list ,(intern (format "hpw-%s-list" wiki-item)))
	    (action-list-actions ,(intern (format "hpw-%s-list-actions" wiki-item)))
	    ;; dynamic actions by setting this hook
	    ;; deal with top and back tips, whose actions must be different
	    ;; from the rest of wiki items
	    (helm-move-selection-after-hook
	     (lambda()(cond
		  ;; Top tip
		  ((equal hpw-top-tip (helm-get-selection))
		   ;; in case of all wiki items, update the callback to "Get to"
		   (cond ((string= ,(symbol-name wiki-item) "wiki")
			  (setcdr (assoc "Get to"
					 action-list-actions)
				  (lambda ()
				    (,(intern (format "hpw-%s" wiki-item))
				     '(4)))))
			 ;; otherwise, just set a new callback
			 (t (setcdr (assq 'action action-list)
				    '(("Get to top" .
				       (lambda ()
					 (,(intern (format "hpw-%s" wiki-item))
					  '(4)))))))))
		  ;; Back tip
		  ((equal hpw-back-tip (helm-get-selection))
		   ;; in case of all wiki items, update the callback to "Get to"
		   (cond ((string= ,(symbol-name wiki-item) "wiki")
			  (setcdr (assoc "Get to" action-list-actions)
				  (lambda ()
				    (,(intern (format "hpw-%s" wiki-item))
				     nil (f-slash (f-parent
						   hpw--directory))))))
			 ;; otherwise, just set a new callback
			 (t (setcdr (assq 'action action-list)
				    '(("Get to upper level" .
				       (lambda (candidate)
					 (,(intern (format "hpw-%s" wiki-item))
					  nil (f-slash (f-parent hpw--directory))))))))))
		  ;; Rest of wiki items
		  (t
		   ;; in case of all wiki items, update the callback to "Get to"
		   (cond ((string= ,(symbol-name wiki-item) "wiki")
			  (setcdr (assoc "Get to" action-list-actions)
				  'hpw-source-action-get-to ))
			 ;; otherwise, just set a new callback
			 (t (setcdr (assq 'action action-list) action-list-actions ))))))))
       (if (equal arg '(4))
	   ;; get all pages by setting `hpw--items-all'
	   (let ((hpw--items-all t)
		 (hpw--directory (projectile-project-root)))
	     (when hpw-debug-mode (message (format "hpw :: New folder is %s" hpw--directory)))
	     (funcall func-to-call))
	 ;; set `hpw--items-all' and `hpw--directory' before
	 ;; calling `helm-projectile-find-item' to limit candidates
	 (let* ((hpw--directory
		 (expand-file-name (or start-directory default-directory)))
		;; deal with the case where at top level
		(hpw--items-all
		 (when (string= (projectile-project-root)
				(expand-file-name hpw--directory))
		   t)))
	   (when hpw-debug-mode (message (format "hpw :: New folder is %s" hpw--directory)))
	   (funcall func-to-call
		    ;; double prefix argument: invalidate cache
		    (if (equal arg '(16)) '(4) nil)))))))

;;;; Wiki

(hpw-item wiki)		;; hpw-wiki

;;;; Category

(hpw-item category)	;; hpw-category

;;;; Pages

(hpw-item pages)	;; hpw-pages

;;;; Index

(defun hpw-index(arg)
  (interactive "P")
  (let ((target-index-file
	 (format "%sindex.%s"
		 (if (equal arg '(4))
		     (projectile-project-root)
		   default-directory )
		 hpw-subcategory-extension)))
    (if (file-exists-p target-index-file)
	(message "hpw :: An index page already exists at current level.")
      ;; otherwise, create new index file
      (hpw--create-index target-index-file))
    (find-file target-index-file)))

;;;; Add wiki

(defun hpw-add-project ()
  "Add current project to the list `hpw-wikis' and activate minor `hpw' mode."
  (interactive)
  (add-to-list 'hpw-wikis
	       (f-filename (projectile-project-root)))
  (hpw-mode 1))

;;;; Help

(defun hpw-help()
  (interactive)
  (browse-url "https://csantosb.gitlab.io/helm-perso-wiki/"))

;;;; Toc

(defun hpw-toc (&optional directory)
  "Insert a table of contents at current point, or update existing."
  (interactive
   (list (expand-file-name default-directory )))
  (save-excursion
    (let ((init (point)))
      (when (re-search-forward "----------" nil t 1)
    	(kill-region init (point))))
    (insert "\n* Table of Contents\n")
    (insert ":PROPERTIES:\n:VISIBILITY: content\n:END:\n\n")
    (hpw-construct-toc directory "**" t t)
    (insert "----------\n")
    (org-set-visibility-according-to-property)))

;;;;; Construct TOC

(defun hpw-construct-toc(currentdir startlevel &optional gitit recursive)
  "Constructs a table of contents of all wiki items found below CURRENTDIR.
Mark current level with STARTLEVEL stars so that the toc appears aligned by org.
When GITIT is non nil, construct a toc to be rendered by gitit instead of org, as they require a
different link format. When RECURSIVE is not nil, proceed recursively on
directories; otherwise, do one level only."

  (let* ((dirs (directory-files currentdir t ".cat$"))           ;; dirs ending by .cat
	 (files (directory-files currentdir t "^[^.]+.page$"))   ;; files ending by .page not beginning by .
	 (top-project-level (projectile-project-root))
	 ;; filter out index pages
	 (files (-mapcat (lambda(arg)
			   (when (not (string-suffix-p "index.page" arg))
			     (list arg)))
			 files)))

    ;; i) page files
    (dolist (myfiles files)  ;; loop over all page files
      ;; read first line of current file
      (with-temp-buffer
	(insert-file-contents myfiles)
	(setq contents (buffer-string))
	(setq gitit-metadata (split-string (car (split-string contents "^\\.\\.\\.$" t)) "\n"))
	(dolist (var gitit-metadata)   ;; get line 'title: '
	  (when (string-prefix-p "title" var)
	    (setq mytitle (replace-regexp-in-string "title: " "" var))))
	(dolist (var gitit-metadata)   ;; get line 'content: '
	  (when (string-prefix-p "content" var)
	    (setq mycontent (replace-regexp-in-string "content: " "" var)))))
      ;; insert gitit/org link to this file, including the first line
      (if gitit
	  ;; gitit link format
	  (insert (format "[[file:/%s][%s]]\t :: %s, %s\n\n"
			  (replace-regexp-in-string ".page$" ""
						    (file-relative-name myfiles top-project-level))
			  (replace-regexp-in-string ".page$" ""
						    (file-relative-name myfiles currentdir))
			  mytitle
			  mycontent))
	;; org link format
	(insert (format "[[%s][%s]]\t\t :: %s :: %s\n\n"
			myfiles
			(file-name-base (file-name-nondirectory myfiles))
			mytitle
			mycontent))))

    ;; ii) cat dirs
    (dolist (mydirs dirs)  ;; loop over all cat dirs
      ;; insert gitit/org link to this folder, including starts to set the hierarchy level
      (if gitit
	  ;; gitit link format
	  (if recursive
	      (insert (format "%s [[file:/%s/index][%s]]\n\n"
			      startlevel
			      (file-relative-name mydirs top-project-level)
			      (replace-regexp-in-string ".cat$" "" (file-relative-name mydirs currentdir))))
	    (insert (format "- [[file:/%s/index][%s]]\n\n"
			    (file-relative-name mydirs top-project-level)
			    (replace-regexp-in-string ".cat$" "" (file-relative-name mydirs currentdir)))))
	;; org link format
	(insert (format "%s [[%s][%s]]\n\n" startlevel mydirs (file-name-base (file-name-nondirectory mydirs)))))
      ;; call this function recursively, increasing the number of start with each level of recursion
      (if recursive
	  (hpw-construct-toc (concat mydirs "/")
			     (concat startlevel "*")
			     gitit recursive)))))

;;; Browse items macro

;; To be called from helm
;; This handles the placement of the point

;;;; Macro

(defmacro hpw-source-action-browse-wiki-item(wiki-item)
  `(defun ,(intern (format "hpw-source-action-browse-%s" wiki-item))(candidate)
     "Following item at point, call `hpw-wiki-item' with relevant parameters."
     (interactive "P")
     ;; call this function
     (funcall (quote ,(intern (format "hpw-%s" wiki-item)))
	      ;; first argument
	      ;; top tip or prefix, set universal arg.; otherwise, send nil
	      (cond ((or (string= hpw-top-tip (helm-get-selection))
			 (equal helm-current-prefix-arg '(4)))
		     '(4)))
	      ;; second argument
	      ;; cat, back tips : send corresponding directory
	      ;; top tip of prefix, send nil
	      (cond ((string-prefix-p hpw-cat-tip (helm-get-selection))
		     (format "%s%s.cat"
			     hpw--directory
			     (replace-regexp-in-string hpw-cat-tip ""
						       (helm-get-selection))))
		    ((string= hpw-back-tip (helm-get-selection))
		     (f-slash (f-parent hpw--directory)))
		    (t hpw--directory)))))

;;;; Generate defuns

(hpw-source-action-browse-wiki-item pages)
(hpw-source-action-browse-wiki-item category)
(hpw-source-action-browse-wiki-item wiki)

;;; Helper

;;;; Create index

(defun hpw--create-index (target-index-file)
  (with-temp-buffer
    ;; TODO: replace by yasnippet
    (insert "---\n")
    (insert "format: Org\n")
    (insert "categories: \n")
    (insert "toc: yes\n")
    (insert "title: \n")
    (insert "content: \n")
    (insert "...\n\n")
    (back-to-indentation)
    (hpw-toc hpw--directory)
    (goto-char (point-max))
    (write-file target-index-file)))

;;; Minor mode

;; Consider current persp project as containing a perso wiki.
;; Define keys to deal with pages/categories as wiki items.

(define-minor-mode hpw-mode
  "Minor mode for dealing with personal wikis."
  :lighter " hpw"
  :keymap (let ((map (make-sparse-keymap)))
	    (define-key map (kbd "M-_")
	      (defhydra hpw-hydra
		(:color blue
			:pre (set-cursor-color "#40e0d0")
			:post (progn
				(set-cursor-color "#ffffff")))
		"Personal Wiki"
		("w" hpw-wiki "wiki")
		("c" hpw-category "category")
		("p" hpw-pages "pages")
		("i" hpw-index "index")
		("t" hpw-toc "toc")
		("?" hpw-help "help")
		("q" nil "quit")))
	    map)
  :global t
  (if hpw-mode
      (progn
	(when hpw-debug-mode (message "hpw :: Personal wiki enabled."))
	(add-hook 'find-file-hook 'hpw--file-link)
	;; when enabled, I advice deft to understand the header of pages
	;; (defadvice deft-parse-summary (before deft-parse-summary-hpw activate)
	;;   (let* ((begin (+ 9 (or (string-match "content: .*$" contents) 0)))
	;; 	 (mycontent (substring contents begin (match-end 0))))
	;;     (setq contents (format "%s\n\n%s" title mycontent))))
	;; (defadvice deft-parse-title (before deft-parse-title-hpw activate)
	;;   (let ((mytitle (substring contents
	;; 			    (+ 7 (or (string-match "title: .*$" contents) 0))
	;; 			    (match-end 0))))
	;;     (setq contents mytitle)))
	)
    (progn
      (when hpw-debug-mode (message "hpw :: Perso wiki disabled."))
      (remove-hook 'find-file-hook 'hpw--file-link)
      ;; remove advice when disabled
      ;; (ad-unadvise 'deft-parse-summary)
      ;; (ad-unadvise 'deft-parse-title)
      )))

;;;; Links

(defun hpw--file-link ()
  (when (and (buffer-file-name)
	     (string= (file-name-extension (buffer-file-name))
		      hpw-subcategory-extension))
    (setq-local org-link-abbrev-alist
		'(("file" . "%(hpw--file-link-parser)")))))

(defun hpw--file-link-parser(arg)
  (let ((newlink (cond (;; A. external to the wiki, absolute from ~
			;; [[file:~/folder1/file.org#Header][my-file]]
			(string-prefix-p "~" arg)
			(format "file:%s%s"
				(replace-regexp-in-string "file:/" "" (replace-regexp-in-string "#.*" "" arg))
				(if (string-match "#" arg)
				    (replace-regexp-in-string ".*#" "::*" arg)
				  "")))
		       (;; B. wiki page, with or without heading
			;; [[file:/category1.cat/category2.cat/my-page#Header][my-page]]
			(string-prefix-p "/" arg)
			(format "file:%s%s.page%s"
				(directory-file-name (projectile-project-root))
				(replace-regexp-in-string "file:/" "" (replace-regexp-in-string "#.*" "" arg))
				(if (string-match "#" arg)
				    (replace-regexp-in-string ".*#" "::*" arg)
				  "")
				))
		       (;; C. local to the page, or to other page's heading
			;; [[#heading][Label]]
			(string-prefix-p "#" arg)
			(format "%s"
				(replace-regexp-in-string "#" "*" arg))
			;; (format "file:%s%s.page%s"
			;; 	(projectile-project-root)
			;; 	(replace-regexp-in-string "file:/" "" (replace-regexp-in-string "#.*" "" arg))
			;; 	(if (string-match "#" arg)
			;; 	    (replace-regexp-in-string ".*#" "::*" arg)
			;; 	  ""))
			)
		       (;; D. external to the wiki, relative from current dir
			;; [[file:folder/my-page::*Header][my-page]]
			t
			(format "file:%s%s" default-directory arg)))))
    (message newlink)
    newlink))

;;;; Persistent action

;; TODO:
(defun hpw-persistent-action ()
  "Persistent action for `helm-do-grep'.
With a prefix arg record CANDIDATE in `mark-ring'."
  (if current-prefix-arg
      (message "hola")
    (message "adios")))

;;;; Sources

;; Two different sources are defined

;; - main, where all pages and categories are shown as item candidates actions:
;;
;; + get to			:: open pages, move to category, get back up one level, get to the top
;; + open deft		:: do it
;; + browse pages		:: find wiki pages
;; + browse categories	:: find wiki categories
;; + create index		:: new index page

;; - fallback, where no candidates are available
;; actions:
;; + create new wiki item, category of page

;;;;; Source: Main

;;;;;; Actions

;;;;;;; action 2: open deft here

;; Launch deft at level

(defun hpw-source-action-open-deft-here (x)
  "Launch deft at current level with current candidate X."
  (let ((deft-directory
	  (if (not (string= x (replace-regexp-in-string hpw-cat-tip "" x)))
	      (format "%s/%s.%s" directory (replace-regexp-in-string hpw-cat-tip "" x)
		      hpw-category-extension)
	    directory))
	(deft-use-filename-as-title nil)
	;; (deft-parse-title-begin 'hpw--deft-locate-title)
	;; (deft-extension hpw-subcategory-extension)
	)
    (deft)
    (setq-local deft-directory deft-directory)))

(defun hpw--deft-locate-title (contents)
  "Locate begin position of CONTENTS."
  (+ 7 (or (string-match "title: .*$" contents) 0)))

;;;;;;; action 5: create index

;; Create or find new index page at current level

(defun hpw-source-action-index-page ()
  "Create a new index page or find existing one.
Independent of CANDIDATE selected."
  (let ((target-index-file
	 (format "%sindex.%s"
		 (cond ((equal helm-current-prefix-arg '(4))
			(projectile-project-root))
		       ;; point in a category
		       ((string-prefix-p hpw-cat-tip (helm-get-selection))
			(format "%s%s.%s/" hpw--directory
				(replace-regexp-in-string hpw-cat-tip
							  "" (helm-get-selection))
				hpw-category-extension))
		       ;; otherwise
		       (t hpw--directory))
		 hpw-subcategory-extension)))
    (if (file-exists-p target-index-file)
	(message "hpw :: an index page already exists at current level")
      ;; otherwise, create new index file
      (hpw--create-index target-index-file))
    (find-file target-index-file)))

;;;;; Source: no candidates found

;;;;;; Actions

;;;;;;; action 1: create

(defun hpw-source-not-found-action-create (candidate)
  "Create new wiki item based on CANDIDATE."
  (cond
   ;; i) categories :: will create a new folder at the current level
   ((string= (file-name-extension candidate) hpw-category-extension)
    (let* ((category (format "%s%s/" hpw--directory candidate))
	   (hpw--directory category))
      ;; create new category
      (dired-create-directory category)
      ;; new index file
      (hpw--create-index (format "%sindex.%s" hpw--directory
				 hpw-subcategory-extension))
      ;; launch again, recursivity
      (hpw-wiki nil hpw--directory)))
   ;; ii) pages :: will open a new buffer
   ((string= (file-name-extension candidate) hpw-subcategory-extension)
    (let ((org-mode-hook nil))
      (find-file (expand-file-name candidate hpw--directory))
      (yas-expand-snippet (format "%s%s$0" hpw--pages-header hpw--toc-header))
      ;; run the hook as usuall
      (org-mode)))
   ;; iii) extensions must be one of .page or .cat
   (t (message (format "Wrong extension, select one of %s"
		       (list hpw-category-extension hpw-subcategory-extension))))))

(provide 'hpw)

;;; hpw.el ends here
