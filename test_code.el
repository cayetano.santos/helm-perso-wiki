
(helm-projectile-command "find-categoryy" hpw-categoryy-list "Find category: ")

;;; A

;; (defvar hpw-categoryy-list
;;   (helm-build-in-buffer-source "Projectile categories"
;;     :data (lambda ()
;; 	    (let ((list-of-categories
;; 		   (condition-case nil
;; 		       (-mapcat 'hpw-categories-filter-function
;; 				(projectile-current-project-dirs))
;; 		     (error nil))))
;; 	      ;; Append a tip to be used to get to the upper level
;; 	      (unless hpw--items-all
;; 		(add-to-list 'list-of-categories hpw-back-tip))
;; 	      ;; Append a tip to be used to get to the top level
;; 	      (unless hpw--items-all
;; 		(add-to-list 'list-of-categories hpw-top-tip))
;; 	      list-of-categories ))
;;     :fuzzy-match helm-projectile-fuzzy-match
;;     :coerce (lambda (candidate)
;; 	      (with-current-buffer (helm-candidate-buffer)
;; 		(format "%s.cat/" (expand-file-name candidate (projectile-project-root)))))
;;     :action-transformer 'helm-find-files-action-transformer
;;     :keymap (let ((map (make-sparse-keymap)))
;; 	      (set-keymap-parent map helm-map)
;; 	      (helm-projectile-define-key map
;; 		(kbd "<left>") #'helm-previous-source
;; 		(kbd "<right>") #'helm-next-source
;; 		(kbd "C-c o") #'helm-projectile-dired-find-dir-other-window
;; 		(kbd "M-e")   #'helm-projectile-switch-to-eshell
;; 		(kbd "C-c f") #'helm-projectile-dired-files-new-action
;; 		(kbd "C-c a") #'helm-projectile-dired-files-add-action
;; 		(kbd "C-s")   #'helm-projectile-grep)
;; 	      map)
;;     :help-message 'helm-ff-help-message
;;     :mode-line helm-read-file-name-mode-line-string
;;     :action '(("Open Dired" . helm-projectile-dired-find-dir)
;; 	      ("Open Dired in other window `C-c o'" . helm-projectile-dired-find-dir)
;; 	      ("Switch to Eshell `M-e'" . helm-projectile-switch-to-eshell)
;; 	      ("Grep in projects `C-s'" . helm-projectile-grep)
;; 	      ("Create Dired buffer from files `C-c f'" . helm-projectile-dired-files-new-action)
;; 	      ("Add files to Dired buffer `C-c a'" . helm-projectile-dired-files-add-action)))
;;   "Helm source for listing project directories.")


;;; B

;; (setq hpw-categoryy-list (copy-tree helm-source-projectile-directories-list))

(setq hpw-categoryy-list
      (helm-build-in-buffer-source "Projectile directories"
	:data (lambda ()
		(condition-case nil
		    (if projectile-find-dir-includes-top-level
			(append '("./") (projectile-current-project-dirs))
		      (projectile-current-project-dirs))
		  (error nil)))
	:fuzzy-match helm-projectile-fuzzy-match
	:coerce 'helm-projectile-coerce-file
	:action-transformer 'helm-find-files-action-transformer
	:keymap (let ((map (make-sparse-keymap)))
		  (set-keymap-parent map helm-map)
		  (helm-projectile-define-key map
		    (kbd "<left>") #'helm-previous-source
		    (kbd "<right>") #'helm-next-source
		    (kbd "C-c o") #'helm-projectile-dired-find-dir-other-window
		    (kbd "M-e")   #'helm-projectile-switch-to-eshell
		    (kbd "C-c f") #'helm-projectile-dired-files-new-action
		    (kbd "C-c a") #'helm-projectile-dired-files-add-action
		    (kbd "C-s")   #'helm-projectile-grep)
		  map)
	:help-message 'helm-ff-help-message
	:mode-line helm-read-file-name-mode-line-string
	:action '(("Open Dired" . helm-projectile-dired-find-dir)
		  ("Open Dired in other window `C-c o'" . helm-projectile-dired-find-dir)
		  ("Switch to Eshell `M-e'" . helm-projectile-switch-to-eshell)
		  ("Grep in projects `C-s'" . helm-projectile-grep)
		  ("Create Dired buffer from files `C-c f'" . helm-projectile-dired-files-new-action)
		  ("Add files to Dired buffer `C-c a'" . helm-projectile-dired-files-add-action)))
      )



(let ((titi (assq 'keymap hpw-categoryy-list)))
  (setcdr titi (add-to-list 'titi '((kbd "C-s") . (lambda () (interactive)
						    (message "hola"))))))


(setcdr (assq-delete-all 'data hpw-categoryy-list) '(a b c d))

(setq hpw-categoryy-list (copy-alist helm-source-projectile-dired-files-list))

(setq hpw-categoryy-list (assq-delete-all 'data hpw-categoryy-list))

(add-to-list 'hpw-categoryy-list '(data (lambda ()
					  (let ((list-of-categories
						 (condition-case nil
						     (-mapcat 'hpw-categories-filter-function
							      (projectile-current-project-dirs))
						   (error nil))))
					    ;; Append a tip to be used to get to the upper level
					    (unless hpw--items-all
					      (add-to-list 'list-of-categories hpw-back-tip))
					    ;; Append a tip to be used to get to the top level
					    (unless hpw--items-all
					      (add-to-list 'list-of-categories hpw-top-tip))
					    list-of-categories ))))


(setcdr (assq 'data hpw-categoryy-list)
	(lambda ()
	  (let ((list-of-categories
		 (condition-case nil
		     (-mapcat 'hpw-categories-filter-function
			      (projectile-current-project-dirs))
		   (error nil))))
	    ;; Append a tip to be used to get to the upper level
	    (unless hpw--items-all
	      (add-to-list 'list-of-categories hpw-back-tip))
	    ;; Append a tip to be used to get to the top level
	    (unless hpw--items-all
	      (add-to-list 'list-of-categories hpw-top-tip))
	    list-of-categories )))


;; (setcdr (assq 'init hpw-categoryy-list)
;; 	(lambda ()
;; 	  (let ((list-of-categories
;; 		 (condition-case nil
;; 		     (-mapcat 'hpw-categories-filter-functionn
;; 			      (projectile-current-project-dirs))
;; 		   (error nil))))
;; 	    ;; Append a tip to be used to get to the upper level
;; 	    (unless hpw--items-all
;; 	      (add-to-list 'list-of-categories hpw-back-tip))
;; 	    ;; Append a tip to be used to get to the top level
;; 	    (unless hpw--items-all
;; 	      (add-to-list 'list-of-categories hpw-top-tip))
;; 	    list-of-categories )))

(setcdr (assq 'coerce hpw-categoryy-list)
	(lambda (candidate)
	  (with-current-buffer (helm-candidate-buffer)
	    (format "%s.cat/" (expand-file-name candidate (projectile-project-root))))))


;; (setcdr (assq 'keymap hpw-categoryy-list)
;; 	'(("Get to top" .
;; 	   (lambda (candidate)
;; 	     (,(intern (format "hpw-%s" wiki-item)) '(4))))))


;; (defun hpw-categories-filter-functionn(x)
;;   (when (and
;; 	 ;; must end by category suffix
;; 	 (string= (f-ext (directory-file-name x))
;; 		  hpw-category-extension)
;; 	 ;; one of
;; 	 (or hpw--items-all
;; 	     ;; proposed category must be a prefix
;; 	     (and
;; 	      (string-prefix-p (file-relative-name
;; 				hpw--directory
;; 				(projectile-project-root))
;; 			       x)
;; 	      ;; avoid showing current level category, show only categories below
;; 	      (> (length (replace-regexp-in-string
;; 			  (expand-file-name hpw--directory)
;; 			  ""
;; 			  (expand-file-name x (projectile-project-root))))
;; 		 5))))
;;     (list (replace-regexp-in-string ".cat/$" "" x))))



;; TODO:

(let ((tmp (copy-alist helm-source-projectile-directories-list)))

  )


(setq hpw-category-list (copy-alist helm-source-projectile-directories-list))
(setq hpw-category-list (assq-delete-all 'name hpw-category-list))
(setq hpw-category-list
      (append
       hpw-category-list
       '((name . "Perso wiki categories (press C-c ? for query help)"))))


;; Replace default actions, adding the new one
;; (let* (;; copy default alist
;;        (helm-source-bibtex-copy
;;	(copy-alist helm-source-bibtex))
;;        ;; extract actions
;;        (helm-source-bibtex-copy-action
;;	(assoc 'action helm-source-bibtex-copy))
;;        ;; append new action
;;        (helm-source-bibtex-copy-action-new (append
;;					    helm-source-bibtex-copy-action
;;					    '(( "Add with Zathura"
;;						. csb/helm-bibtex-open-with-zathura)))))
;;   ;; delete actions from original
;;   (assq-delete-all 'action helm-source-bibtex)
;;   ;; add new actions
;;   (setq helm-source-bibtex
;;	(append
;;	 helm-source-bibtex
;;	 (list helm-source-bibtex-copy-action-new))))
