#+TITLE:     Helm personal Wiki
#+AUTHOR:    Cayetano Santos
#+LANGUAGE:  en
#+DESCRIPTION: personal wiki
#+OPTIONS:   H:2 num:nil toc:2

-----

Implements a minor mode to deal with a very simple personal wiki, providing
a way to navigate a hierarchy of categories and pages (wiki items).

The wiki may exist in parallel to other project files (to document them, for
example). The minor mode ignores files not considered as wiki items.

The navigating facility provided here extends and is based on the combined
use of =helm=, =perspective= and =projectile=. Finally, the wiki structure
aims at being compatible with [[http://jblevins.org/projects/deft/][deft]] and [[http://gitit.net/][gitit]].

See this projects [[https://csantosb.gitlab.io/helm-perso-wiki/][documentation]] page for details.

* License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
